from django import template
from django.urls import reverse
from events.models import OneOffEventsListPage
from share.views import shareHelloUrl

register = template.Library()


@register.inclusion_tag('share/tags/share_tag.html', takes_context=True)
def share_one_off_event(context):
    request = context['request']
    user = getattr(request, 'user', None)
    parent = OneOffEventsListPage.objects.first()
    url = reverse("wagtailadmin_pages:add_subpage", args=[parent.id])
    if not user or not user.is_authenticated:
        url = shareHelloUrl(url)
    return {
        'description': "Share a One Off Event",
        'url':         url,
    }
