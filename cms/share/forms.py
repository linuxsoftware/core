import re
import unicodedata
from django import forms
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group, Permission
from wagtail.wagtailusers.models import UserProfile

User = get_user_model() # spoiler: it's dashboard.models.User ;-)

class HelloForm(forms.ModelForm):
    class Meta:
        model = User
        fields = [
            'first_name',
            'last_name',
            'email',
            'phone',
            'password1',
        ]
        required_css_class = 'required'
        widgets = {
        }
    error_messages = {
        'duplicate_username': "A user with that name already exists.",
    }

    first_name = forms.CharField(required=True, label='First Name')
    last_name = forms.CharField(required=True, label='Last Name')
    email = forms.EmailField(required=True, label='Email')
    phone = forms.CharField(required=False, label='Phone')
    password1 = forms.CharField(
        label='Password', required=False,
        widget=forms.PasswordInput(attrs={'autocomplete': 'new-password'}),
        help_text="A password is optional, but making changes will be easier if you set one."
    )

    def _clean_fields(self):
        super()._clean_fields()
        self._clean_username()

    def _clean_username(self):
        username = "{first_name}.{last_name}".format_map(self.cleaned_data)
        username = unicodedata.normalize('NFKC', username)
        username = re.sub(r'[^\w\.]', '', username, flags=re.U).strip().lower()
        if User.objects.filter(username=username).exists():
            self.add_error(None,
                           forms.ValidationError(self.error_messages['duplicate_username'],
                                                 code='duplicate_username'))
        self.cleaned_data['username'] = username

    def save(self, commit=True):
        user = super().save(commit=False)
        user.username = self.cleaned_data['username']
        password = self.cleaned_data['password1']
        if password:
            user.set_password(password)
        else:
            user.set_unusable_password()
        user.is_active = True
        user.is_staff = False
        user.is_superuser = False
        user.wagtail_userprofile = UserProfile()

        if commit:
            user.save()
            self._addGroups(user)
            self.save_m2m()
        return user

    def _addGroups(self, user):
        community, _ = Group.objects.get_or_create(name="Community Contacts")
        accessAdmin = Permission.objects.filter(codename="access_admin").get()
        community.permissions.add(accessAdmin)
        personal, _ = Group.objects.get_or_create(name=user.username)
        user.groups.add(community, personal)
