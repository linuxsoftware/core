from urllib.parse import urlparse, urlunparse, quote_plus
from django.views.generic.edit import FormView
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib import auth
from django.utils.http import is_safe_url
from django.http import QueryDict
from .forms import HelloForm

def loginUrl(next=None):
    url=reverse('wagtailadmin_login')
    if next:
        url_parts = list(urlparse(url))
        querystring = QueryDict(url_parts[4], mutable=True)
        querystring[auth.REDIRECT_FIELD_NAME] = next
        url_parts[4] = querystring.urlencode(safe='/')
        url = urlunparse(url_parts)
    return url

def shareHelloUrl(next):
    return "{}?{}={}".format(reverse("share_hello"),
                             'next',
                             quote_plus(next))

class HelloView(FormView):
    form_class = HelloForm
    template_name = 'share/hello.html'

    def dispatch(self, request, *args, **kwargs):
        auth.logout(request)
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        redirectUrl = self._getRedirectUrl()
        kwargs['loginUrl'] = loginUrl(redirectUrl)
        if redirectUrl:
            kwargs['next'] = redirectUrl
        return super().get_context_data(**kwargs)

    def _getRedirectUrl(self):
        # Return the user-originating redirect URL if it's safe.
        redirect_to = (self.request.POST.get('next', '') or
                       self.request.GET.get('next', ''))
        url_is_safe = is_safe_url(url=redirect_to,
                                  allowed_hosts=None, # [self.request.get_host()?]
                                  require_https=self.request.is_secure())
        return redirect_to if url_is_safe else ''

    def form_valid(self, form):
        # This method is called when valid form data has been POSTed.
        # if we are already authenticated do nothing
        if self.request.user.is_authenticated:
            # shouldn't happen anyway
            return HttpResponseRedirect(self.get_success_url())

        user = form.save()
        self.request.user = user
        self.request.session.set_expiry(3600*24*30)
        auth.login(self.request, user)
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        url = self._getRedirectUrl() or "/"
        return url
