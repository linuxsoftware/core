from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'hello/$', views.HelloView.as_view(), name='share_hello'),
]
