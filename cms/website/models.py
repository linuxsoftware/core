# ------------------------------------------------------------------------------
# Website models
# These are pages and things used across the site
# ------------------------------------------------------------------------------

from django.db import models
from wagtail.wagtailcore.models import Page
from wagtail.wagtailcore.fields import RichTextField, StreamField
from wagtail.wagtailadmin.edit_handlers import (FieldPanel, MultiFieldPanel,
        PageChooserPanel, StreamFieldPanel)
from wagtail.wagtaildocs.edit_handlers import DocumentChooserPanel
from wagtail.contrib.settings.models import BaseSetting, register_setting
from .blocks import (CardBlock, ColourBlock, HeadingBlock, ImageBlock,
        ParagraphBlock, SectionBlock, SliderBlock, SubheadingBlock, TableBlock,
        VideoBlock)

# ------------------------------------------------------------------------------
# Top Level Page base
# ------------------------------------------------------------------------------

class TopLevelPage(Page):
    class Meta:
        abstract = True

    parent_page_types = ['home.HomePage']

    @classmethod
    def can_create_at(cls, parent):
        # You can only create one of these pages
        return super().can_create_at(parent) and not cls.objects.exists()

# ------------------------------------------------------------------------------
# Links
# ------------------------------------------------------------------------------

class LinkFields(models.Model):
    class Meta:
        abstract = True

    link_external = models.URLField("External link", blank=True)
    link_page = models.ForeignKey(
        'wagtailcore.Page',
        null=True,
        blank=True,
        related_name='+'
    )
    link_document = models.ForeignKey(
        'wagtaildocs.Document',
        null=True,
        blank=True,
        related_name='+'
    )

    panels = [
        FieldPanel('link_external'),
        PageChooserPanel('link_page'),
        DocumentChooserPanel('link_document'),
    ]

    @property
    def link(self):
        if self.link_page:
            return self.link_page.url
        elif self.link_document:
            return self.link_document.url
        else:
            return self.link_external

class RelatedLink(LinkFields):
    class Meta:
        abstract = True

    title = models.CharField(max_length=255, help_text="Link title")

    panels = [
        FieldPanel('title'),
        MultiFieldPanel(LinkFields.panels, "Link"),
    ]

# ------------------------------------------------------------------------------
# Website Pages
# ------------------------------------------------------------------------------

class StreamPage(Page):
    class Meta:
        verbose_name = "Content Stream Page"

    content = StreamField([('heading',   HeadingBlock()),
                           ('section',   SectionBlock()),
                           ('slider',    SliderBlock()),
                           ('paragraph', ParagraphBlock()),
                           ('colour',    ColourBlock()),
                           ('image',     ImageBlock()),
                           ('video',     VideoBlock()),
                           ('card',      CardBlock()),
                           ('table',     TableBlock()),
                          ], null=True, blank=True)
    content.help_text = "A page made up of blocks of text, images, video, etc."

    content_panels = Page.content_panels + [
        StreamFieldPanel('content')
        ]


class PlainPage(Page):
    class Meta:
        verbose_name = "Plain Text Page"

    content = RichTextField(default='', blank=True)
    content.help_text = "An area of text for whatever you like"

    content_panels = Page.content_panels + [
        FieldPanel('content', classname="full"),
        ]


# ------------------------------------------------------------------------------
# Settings
# ------------------------------------------------------------------------------

@register_setting(icon='mail')
class AddressSettings(BaseSetting):
    phys_care_of = models.CharField(max_length=80, blank=True,
                                    verbose_name="Care Of")
    phys_address1 = models.CharField(max_length=80,
                                     verbose_name="Address1")
    phys_address2 = models.CharField(max_length=80, blank=True,
                                     verbose_name="Address2")
    phys_town = models.CharField(max_length=80,
                                 verbose_name="Town")
    phys_postcode = models.CharField(max_length=6,
                                     verbose_name="Postcode")
    phys_latlng = models.CharField(max_length=20, blank=True,
                                   verbose_name="Lat,Lng")
    google_place_id = models.CharField(max_length=30, blank=True,
                                       verbose_name="Google Place Id")
    post_care_of = models.CharField(max_length=80, blank=True,
                                    verbose_name="Care Of")
    post_address1 = models.CharField(max_length=80, blank=True,
                                     verbose_name="Address1")
    post_address2 = models.CharField(max_length=80, blank=True,
                                     verbose_name="Address2")
    post_town = models.CharField(max_length=80, blank=True,
                                 verbose_name="Town")
    post_postcode = models.CharField(max_length=6, blank=True,
                                     verbose_name="Postcode")
    phone = models.CharField(max_length=30, blank=True)

    panels = [
        MultiFieldPanel([
            FieldPanel('phys_care_of'),
            FieldPanel('phys_address1'),
            FieldPanel('phys_address2'),
            FieldPanel('phys_town'),
            FieldPanel('phys_postcode'),
            FieldPanel('phys_latlng'),
            #FieldPanel('google_place_id')
            ],
            heading="Physical Address"),
        MultiFieldPanel([
            FieldPanel('post_care_of'),
            FieldPanel('post_address1'),
            FieldPanel('post_address2'),
            FieldPanel('post_town'),
            FieldPanel('post_postcode')],
            heading="Postal Address"),
        FieldPanel('phone')
    ]

    class Meta:
        verbose_name = "Address"
        verbose_name_plural = "Addresses"

@register_setting(icon='link')
class LinksSettings(BaseSetting):
    vimeo  = models.URLField(blank=True)
    facebook  = models.URLField(blank=True)
    twitter  = models.URLField(blank=True)
    instagram  = models.URLField(blank=True)

    panels = [
        FieldPanel('vimeo'),
        FieldPanel('facebook'),
        FieldPanel('twitter'),
        FieldPanel('instagram'),
    ]

    class Meta:
        verbose_name = "External Links"
        verbose_name_plural = "External Links"
