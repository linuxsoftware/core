# ------------------------------------------------------------------------------
# Extra widgets
# ------------------------------------------------------------------------------
import sys
import json
import datetime as dt
from wagtail.wagtailadmin.widgets import AdminTimeInput
from django.contrib.staticfiles.templatetags.staticfiles import static
from django.forms import Media
from website.utils import timeFormat

# ------------------------------------------------------------------------------
class Time12hrInput(AdminTimeInput):
    """Display and Edit time fields in a 12hr format"""
    def __init__(self, attrs=None):
        super().__init__(attrs=attrs)

    def format_value(self, value):
        return timeFormat(value)

    def render_js_init(self, id_, name, value):
        return "initTime12hrChooser({});".format(json.dumps(id_))

    @property
    def media(self):
        return Media(js=[static("website/js/time12hr_admin.js")])

# ------------------------------------------------------------------------------
