import datetime as dt
from django.utils import dateformat


# ------------------------------------------------------------------------------
def timeFormat(time_from, time_to=None, prefix="", infix="to "):
    # e.g. 10am
    retval = ""
    if time_from != "" and time_from is not None:
        retval += prefix
        retval += dateformat.time_format(time_from, "fA ").lower()
    if time_to != "" and time_to is not None:
        retval += infix
        retval += format(dateformat.time_format(time_to, "fA").lower())
    return retval

def dateFormat(when):
    # e.g. Friday, 14th of April
    if when is not None:
        return dateformat.format(when, "l jS \\o\\f F")
    else:
        return ""

# ------------------------------------------------------------------------------
