# ------------------------------------------------------------------------------
# Website blocks
# ------------------------------------------------------------------------------

from django.db import models
from wagtail.wagtailcore.blocks import (CharBlock, ChoiceBlock, RichTextBlock,
    StructBlock, StreamBlock, PageChooserBlock, URLBlock)
from wagtail.contrib.table_block.blocks import TableBlock
from wagtail.wagtailimages.blocks import ImageChooserBlock
from wagtail.wagtailembeds.blocks import EmbedBlock

# ------------------------------------------------------------------------------
# Blocks
# ------------------------------------------------------------------------------

class HeadingBlock(CharBlock):
    class Meta:
        template = "website/blocks/heading.html"
        classname="full title"
        icon = 'title'

class SubheadingBlock(CharBlock):
    class Meta:
        template = "website/blocks/subheading.html"
        classname="title"
        icon = 'title'

class ParagraphBlock(RichTextBlock):
    class Meta:
        template = "website/blocks/paragraph.html"
        classname="full"
        icon = 'pilcrow'

class ColourBlock(StructBlock):
    class Meta:
        template = "website/blocks/colour.html"
        icon = 'doc-full-inverse'

    colour = ChoiceBlock(choices=[("kowhai_smalt",         "Kowhai / Smalt"),
                                  ("alabaster_smalt",      "Alabaster / Smalt"),
                                  ("kowhai_jewel",         "Kowhai / Jewel"),
                                  ("alabaster_jewel",      "Alabaster / Jewel"),
                                  ("white_jewel",          "White / Jewel"),
                                  ("midnight_forest",      "Midnight / Forest"),
                                  ("alabaster_forest",     "Alabaster / Forest"),
                                  ("forest_crusoe",        "Forest / Crusoe"),
                                  ("kowhai_crusoe",        "Kowhai / Crusoe"),
                                  ("alabaster_crusoe",     "Alabaster / Crusoe"),
                                  ("smalt_kowhai",         "Smalt / Kowhai"),
                                  ("jewel_kowhai",         "Jewel / Kowhai"),
                                  ("crusoe_kowhai",        "Crusoe / Kowhai"),
                                  ("midnight_kowhai",      "Midnight / Kowhai"),
                                  ("freespeech_kowhai",    "Freespeech / Kowhai"),
                                  ("forest_midnight",      "Forest / Midnight"),
                                  ("kowhai_midnight",      "Kowhai / Midnight"),
                                  ("alabaster_midnight",   "Alabaster / Midnight"),
                                  ("smalt_alabaster",      "Smalt / Alabaster"),
                                  ("jewel_alabaster",      "Jewel / Alabaster"),
                                  ("forest_alabaster",     "Forest / Alabaster"),
                                  ("crusoe_alabaster",     "Crusoe / Alabaster"),
                                  ("midnight_alabaster",   "Midnight / Alabaster"),
                                  ("freespeech_alabaster", "Freespeech / Alabaster"),
                                  ("kowhai_freespeech",    "Kowhai / Freespeech"),
                                  ("midnight_freespeech",  "Midnight / Freespeech"),
                                  ("alabaster_freespeech", "Alabaster / Freespeech"),
                                  ("white_freespeech",     "White / Freespeech"),
                                  ("smalt_white",          "Smalt / White"),
                                  ("jewel_white",          "Jewel / White"),
                                  ("forest_white",         "Forest / White"),
                                  ("crusoe_white",         "Crusoe / White"),
                                  ("midnight_white",       "Midnight / White"),
                                  ("freespeech_white",     "Freespeech / White")],
                             default="crusoe_kowhai",
                             classname="web-colour-field",
                             required=True)
    content = RichTextBlock(classname="full", required=True)


    def get_context(self, value, parent_context=None):
        context = super().get_context(value, parent_context)
        value['foreground'], _, value['background'] = value.get('colour', "").partition('_')
        return context

    def js_initializer(self):
        return "initColourSelect"

class ImageBlock(StructBlock):
    class Meta:
        template = "website/blocks/image.html"
        icon = 'image'

    image = ImageChooserBlock(required=True)
    sizing = ChoiceBlock(choices=[("original",  "Original"),
                                  ("1000x1000", "Large"),
                                  ("600x500",   "Medium"),
                                  ("300x240",   "Smallish"),
                                  ("250x200",   "Small"),
                                  ("100x100",   "Thumbnail")],
                         default="original",
                         classname="web-half-field",
                         required=True)
    format = ChoiceBlock(choices=[("left",       "Left"),
                                  ("right",      "Right"),
                                  ("centre",     "Centre"),
                                  ("full-width", "Full")],
                         default="left",
                         classname="web-half-field",
                         required=True)
    caption = CharBlock(required=False)

class VideoBlock(StructBlock):
    class Meta:
        template = "website/blocks/video.html"
        icon = 'media'

    video = EmbedBlock(label="Link to video")
    sizing = ChoiceBlock(choices=[("original", "Original"),
                                  ("1000",     "Large"),
                                  ("600",      "Medium"),
                                  ("480",      "Small")],
                         default="original",
                         classname="web-half-field",
                         required=True)
    format = ChoiceBlock(choices=[("left",       "Left"),
                                  ("right",      "Right"),
                                  ("centre",     "Centre"),
                                  ("full-width", "Full")],
                         default="left",
                         classname="web-half-field",
                         required=True)
    caption = CharBlock(required=False)

class CardBlock(StructBlock):
    class Meta:
        template = "website/blocks/card.html"
        icon = 'doc-full'

    title = CharBlock(classname="title", required=False)
    figure = ImageChooserBlock(required=True)
    figure_caption = CharBlock(required=False)
    content = RichTextBlock(classname="full", required=True)
#    link = StructBlock([('external_link', URLBlock(required=False)),
#                        ('link_page', PageChooserBlock(required=False)),
#                       ], null=True, blank=True)

class SliderBlock(StructBlock):
    class Meta:
        template = "website/blocks/slider.html"
        icon = 'arrow-down'

    title = CharBlock(classname="title", required=True)
    content = StreamBlock([('subheading', SubheadingBlock()),
                           ('paragraph',  ParagraphBlock()),
                           ('colour',     ColourBlock()),
                           ('image',      ImageBlock()),
                           ('video',      VideoBlock()),
                           ('card',       CardBlock()),
                           ('table',      TableBlock()),
                          ], null=True, blank=True)

class SectionBlock(StructBlock):
    class Meta:
        template = "website/blocks/section.html"
        icon = 'pilcrow'

    title = CharBlock(classname="title")
    content = StreamBlock([('subheading', SubheadingBlock()),
                           ('paragraph',  ParagraphBlock()),
                           ('colour',     ColourBlock()),
                           ('image',      ImageBlock()),
                           ('video',      VideoBlock()),
                           ('card',       CardBlock()),
                           ('table',      TableBlock()),
                           ('slider',     SliderBlock()),
                          ], null=True, blank=True)

# ------------------------------------------------------------------------------
# ------------------------------------------------------------------------------

