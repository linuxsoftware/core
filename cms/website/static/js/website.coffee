#---------------------------------------------------------------------------
# Site wide scripts
#---------------------------------------------------------------------------

initSearchToggle = () ->
    slideSpeed = 350
    search = $(".top-bar .search-bar")
    qInput = $("input#top-search-box-q")
    menu = $(".top-bar .top-menu")
    search.hide()
    savedFocus = prevFocus = $()
    $(document).on 'focusin', ':input', () ->
        prevFocus = $(this)
        return
    $(".top-menu .open-search").click () ->
        savedFocus = prevFocus
        search.height(parseInt(menu.height(),10))
        search.slideDown slideSpeed, () ->
            qInput.val("").focus()
            return
        menu.slideUp(slideSpeed)
        return false
    $(".search-bar .close-search").click () ->
        qInput.val("")
        search.slideUp(slideSpeed)
        menu.slideDown(slideSpeed)
        if prevFocus.is(qInput)
            savedFocus.focus()
        else
            prevFocus.focus()
        return false
    return

enableSliders = () ->
    $(".web-slider-heading").click (ev) ->
        slider = $(this).closest(".web-slider")
        if slider.hasClass("web-slider-down")
            slider.removeClass("web-slider-down").addClass("web-slider-up")
            slider.find(".web-slider-drawer").slideUp(200)
        else
            slider.removeClass("web-slider-up").addClass("web-slider-down")
            slider.find(".web-slider-drawer").slideDown(200)
        return
    return

$ ->
    initSearchToggle()
    enableSliders()
    return


