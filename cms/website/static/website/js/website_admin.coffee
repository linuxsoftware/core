#---------------------------------------------------------------------------
# Admin script included on all admin pages
#---------------------------------------------------------------------------

class ColourSelect
    constructor: (prefix) ->
        @select = $("##{prefix}-colour")
        @_matchColours()
        return

    enable: () ->
        @select.on 'change', () =>
            @_matchColours()
            return
        return

    _matchColours: () ->
        colour = @select.find("option:selected").val()
        @select.attr('class', '').addClass(colour)
        return

@initColourSelect = (prefix) ->
    widget = new ColourSelect(prefix)
    widget.enable()
    return
