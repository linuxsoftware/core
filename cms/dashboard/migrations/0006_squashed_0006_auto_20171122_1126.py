# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2017-11-24 20:16
from __future__ import unicode_literals

from django.db import migrations

# These migrations generated by django_custom_user_migration
# are not needed for a fresh install
#    dashboard.migrations.0006_auto_20171122_1126

class Migration(migrations.Migration):

    replaces = [('dashboard', '0006_auto_20171122_1126')]

    dependencies = [
        ('dashboard', '0005_auto_20171122_1124'),
    ]

    operations = [
    ]
