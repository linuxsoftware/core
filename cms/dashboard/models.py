from django.db import models
from django.contrib.auth.models import AbstractUser
#from django_custom_user_migration.models import AbstractUser

class User(AbstractUser):
    email = models.EmailField(blank=False, unique=True)
    phone = models.CharField(max_length=30, blank=True)


