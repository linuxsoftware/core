from django.contrib.auth.models import Permission
from django.db.models import Q
from wagtail.wagtailcore import hooks
from wagtail.wagtailcore.models import GroupPagePermission
@hooks.register('register_permissions')
def register_permissions():
    return Permission.objects.filter(content_type__app_label='dashboard',
                                     codename='menu_search')

@hooks.register('construct_homepage_panels')
def hide_site_summary(request, panels):
    panels[:] = [panel for panel in panels if panel.name != "site_summary"]

@hooks.register('construct_explorer_page_queryset')
def hide_uneditable_pages(parent_page, pages, request):
    # NB this will even hide some pages that we have other permissions on
    # (e.g. add or publish)
    user = getattr(request, 'user', None)
    if not user or not user.is_active:
        return pages.none()
    if user.is_superuser:
        return pages
    perms = GroupPagePermission.objects.filter(group__user=user)
    modifyPagePerms = [perm for perm in perms
                       .filter(permission_type__in=['add', 'publish', 'lock'])]
    editQry = Q(group_permissions__in=modifyPagePerms)
    for perm in perms.filter(permission_type = 'edit').select_related('page'):
        editQry.add(Q(path__startswith=perm.page.path), Q.OR)
    if parent_page.permissions_for_user(user).can_add_subpage():
        editQry.add(Q(owner=user), Q.OR)
    pages = pages.filter(editQry)
    return pages
