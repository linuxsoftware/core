from django import template
from django.conf import settings
from wagtail.wagtailadmin.templatetags.wagtailadmin_tags import menu_search

register = template.Library()

@register.simple_tag()
def get_wagtail_site_name():
    return getattr(settings, 'WAGTAIL_SITE_NAME', 'Wagtail')

@register.simple_tag(takes_context=True)
def dashboard_menu_search(context):
    request = context.get('request')
    if request and request.user.has_perms(['dashboard.menu_search']):
        return menu_search(context)
    else:
        return ""



