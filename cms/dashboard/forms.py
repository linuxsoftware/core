from django import forms
from wagtail.wagtailusers.forms import UserEditForm as BaseUserEditForm
from wagtail.wagtailusers.forms import UserCreationForm as BaseUserCreationForm


class UserEditForm(BaseUserEditForm):
    phone = forms.CharField(required=False)

class UserCreationForm(BaseUserCreationForm):
    phone = forms.CharField(required=False)
