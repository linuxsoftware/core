# ------------------------------------------------------------------------------
# Homepage models
# ------------------------------------------------------------------------------

from django.db import models

from wagtail.wagtailcore.models import Page, Orderable
from wagtail.wagtailcore.fields import RichTextField
from wagtail.wagtailadmin.edit_handlers import FieldPanel, MultiFieldPanel, \
    InlinePanel, PageChooserPanel
from wagtail.wagtailimages.edit_handlers import ImageChooserPanel
from modelcluster.fields import ParentalKey


class HomePageHighlight(Orderable):
    homepage = ParentalKey('home.HomePage', related_name='highlights')
    title = models.CharField("Title", max_length=80, blank=True)
    blurb = RichTextField(default='', blank=True)
    image = models.ForeignKey('wagtailimages.Image',
                              null=True,
                              blank=True,
                              on_delete=models.SET_NULL,
                              related_name='+')
    image.help_text = "Ideally this image would be 212x130"
    page  = models.ForeignKey('wagtailcore.Page',
                              null=True,
                              blank=True,
                              related_name='+')

    panels = [FieldPanel('title', classname="full title"),
              FieldPanel('blurb', classname="full"),
              ImageChooserPanel('image'),
              PageChooserPanel('page'),]


class HomePage(Page):
    parent_page_types = []
    subpage_types = ["website.StreamPage",
                     "website.PlainPage",
                     "contact.ContactPage",
                     "groups.DirectoryPage",
                     "events.AllEventsListPage",
                     "events.OneOffEventsListPage",
                     "events.RegularEventsListPage",
                     "events.AllEventsCalendarPage",
                     "events.OneOffEventsCalendarPage",
                     "events.RegularEventsCalendarPage" ]

    background_image = models.ForeignKey('wagtailimages.Image',
                                         verbose_name="Image",
                                         null=True,
                                         blank=True,
                                         on_delete=models.SET_NULL,
                                         related_name='+')
    background_image.help_text = "A large image (at least 2560x1440) to "\
                                 "cover the background"
    background_attribution = models.CharField('Attribution', max_length=80, blank=True)
    background_attribution.help_text = "Attribution for the background "\
                                       "image used"
    background_attribution_link  = models.URLField("Link", blank=True)
    background_attribution_link.help_text = "Link to the original copy"

    welcome = RichTextField(default='', blank=True)
    welcome.help_text = "A short introductory message"
    content = RichTextField(default='', blank=True)
    content.help_text = "An area of text for whatever you like"


    # HomePage is a bit different to other pages as the title is not displayed
    content_panels = [
        MultiFieldPanel([ImageChooserPanel('background_image'),
                         FieldPanel('background_attribution'),
                         FieldPanel('background_attribution_link')],
                        heading="Background"),
        FieldPanel('welcome', classname="full"),
        MultiFieldPanel([InlinePanel('highlights')],
                        heading="Highlights",
                        classname="collapsible"),
        FieldPanel('content', classname="full"),
        ]
