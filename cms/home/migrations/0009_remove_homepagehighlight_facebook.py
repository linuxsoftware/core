# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2017-10-11 01:27
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0008_auto_20170925_1430'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='homepagehighlight',
            name='facebook',
        ),
    ]
