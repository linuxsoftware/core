from .base import *

# SECURITY WARNING: don't run with debug turned on in production!
#DEBUG = True
#from django.contrib.messages import constants as message_constants
#MESSAGE_LEVEL = message_constants.DEBUG
DEBUG = False

ALLOWED_HOSTS += ['core.linuxsoftware.nz', '.core.linuxsoftware.nz']

SESSION_COOKIE_HTTPONLY=True
SESSION_COOKIE_DOMAIN = '.core.linuxsoftware.nz'

try:
    from .local import *
except ImportError:
    pass
