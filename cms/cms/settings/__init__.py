# settings/__init__.py

import os

ENV = os.getenv('COREENV', 'dev')

if ENV == 'dev':
    from .dev import *
elif ENV == 'uat':
    from .uat import *
elif ENV == 'prd':
    from .prd import *
