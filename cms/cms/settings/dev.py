from .base import *

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True
from django.contrib.messages import constants as message_constants
MESSAGE_LEVEL = message_constants.DEBUG

EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

ALLOWED_HOSTS += ['.localhost', '127.0.0.1', '[::1]', '.core.nikau', 'core.nikau']
SESSION_COOKIE_DOMAIN = '.core.nikau'

try:
    from .local import *
except ImportError:
    pass

# Now using a different db for development
DATABASES['default']['NAME'] = "dev_core"
