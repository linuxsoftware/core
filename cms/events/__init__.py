# ------------------------------------------------------------------------------
# Joyous initialization
# ------------------------------------------------------------------------------
import calendar
from django.conf import settings

if not hasattr(settings, 'JOYOUS_DAY_OF_WEEK_START'):
    settings.JOYOUS_DAY_OF_WEEK_START = "Sunday"
if not hasattr(settings, 'JOYOUS_DEFAULT_EVENTS_VIEW'):
    settings.JOYOUS_DEFAULT_EVENTS_VIEW = "Monthly"
if not hasattr(settings, 'JOYOUS_HOLIDAYS'):
    settings.JOYOUS_HOLIDAYS = ""

# ------------------------------------------------------------------------------
# Use "Monday" as the first day of the week following European convention
# and ISO 8601, OR use "Sunday" as the first day of the week following Jewish 
# tradition
#     https://en.wikipedia.org/wiki/Week#Week_numbering
#     http://www.cjvlang.com/Dow/SunMon.html
# Keep this the same as the JQueryDatePicker to avoid confusion
# see  http://xdsoft.net/jqplugins/datetimepicker
if settings.JOYOUS_DAY_OF_WEEK_START == "Monday":
    calendar.setfirstweekday(0)
else:
    calendar.setfirstweekday(1)


