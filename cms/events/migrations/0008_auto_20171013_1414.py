# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2017-10-13 01:14
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion
import wagtail.wagtailcore.fields


class Migration(migrations.Migration):

    dependencies = [
        ('wagtailcore', '0040_page_draft_title'),
        ('events', '0007_auto_20171013_0944'),
    ]

    operations = [
        migrations.CreateModel(
            name='OneOffEventsListPage',
            fields=[
                ('page_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='wagtailcore.Page')),
                ('intro', wagtail.wagtailcore.fields.RichTextField(blank=True)),
            ],
            options={
                'verbose_name': 'One Off Upcoming Events Page',
            },
            bases=('wagtailcore.page',),
        ),
        migrations.CreateModel(
            name='RegularEventsListPage',
            fields=[
                ('page_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='wagtailcore.Page')),
                ('intro', wagtail.wagtailcore.fields.RichTextField(blank=True)),
            ],
            options={
                'verbose_name': 'Regular Upcoming Events Page',
            },
            bases=('wagtailcore.page',),
        ),
        migrations.RenameModel(
            old_name='EventIndexPage',
            new_name='AllEventsListPage',
        ),
    ]
