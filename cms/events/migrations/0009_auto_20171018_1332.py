# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2017-10-18 00:32
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('events', '0008_auto_20171013_1414'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='multidayeventpage',
            options={'verbose_name': 'Multiday Event Page'},
        ),
        migrations.AlterModelOptions(
            name='recurringeventpage',
            options={'verbose_name': 'Recurring Event Page'},
        ),
        migrations.AlterModelOptions(
            name='simpleeventpage',
            options={'verbose_name': 'One-Off Event Page'},
        ),
    ]
