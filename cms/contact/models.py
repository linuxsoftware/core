# ------------------------------------------------------------------------------
# Contact models
# ------------------------------------------------------------------------------

from django.db import models
from django.core.validators import MinValueValidator, MaxValueValidator
from wagtail.wagtailadmin.edit_handlers import FieldPanel, MultiFieldPanel, \
    FieldRowPanel, InlinePanel, TabbedInterface, ObjectList
from wagtail.wagtailcore.fields import RichTextField
from wagtail.wagtailforms.models import AbstractEmailForm, AbstractFormField
from wagtail.wagtailforms.edit_handlers import FormSubmissionsPanel
from modelcluster.fields import ParentalKey
from website.models import TopLevelPage

class ContactPageField(AbstractFormField):
    page = ParentalKey('ContactPage', related_name='form_fields')


class ContactPage(AbstractEmailForm, TopLevelPage):
    map_zoom = models.IntegerField(default=17, validators=[MinValueValidator(1), MaxValueValidator(21)])
    more_details = RichTextField("More Contact Details", blank=True)
    intro = RichTextField(blank=True)
    thank_you_text = RichTextField(blank=True)

    content_panels = AbstractEmailForm.content_panels + [
        FieldPanel('map_zoom'),
        FieldPanel('more_details'),
        MultiFieldPanel([
            FieldPanel('intro', classname="full"),
            InlinePanel('form_fields', label="Form fields"),
            FieldPanel('thank_you_text', classname="full"),
            MultiFieldPanel([
                FieldRowPanel([
                    FieldPanel('from_address', classname="col6"),
                    FieldPanel('to_address', classname="col6"),
                ]),
                FieldPanel('subject'),
            ], "Email"),
        ], "Contact Form"),
    ]

    submissions_panels = [
        FormSubmissionsPanel(),
    ]

    edit_handler = TabbedInterface([
        ObjectList(content_panels, heading='Content'),
        ObjectList(AbstractEmailForm.promote_panels, heading='Promote'),
        ObjectList(AbstractEmailForm.settings_panels, heading='Settings', classname="settings"),
        ObjectList(submissions_panels, heading='Submissions'),
    ])

