# ------------------------------------------------------------------------------
# Groups hooks
# ------------------------------------------------------------------------------

from wagtail.contrib.modeladmin.options import ModelAdmin
from wagtail.contrib.modeladmin.options import modeladmin_register
from groups.models import GroupCategory

class GroupCategoryAdmin(ModelAdmin):
    model = GroupCategory
    menu_icon = 'tag'
    menu_label = 'Group Categories'
    menu_order = 1200
    add_to_settings_menu = True
    exclude_from_explorer = False
    list_display = ('code', 'name')
    list_filter = ()
    search_fields = ('name',)

modeladmin_register(GroupCategoryAdmin)

