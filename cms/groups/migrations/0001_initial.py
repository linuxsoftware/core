# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2017-08-09 01:52
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion
import wagtail.wagtailcore.fields


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('wagtailcore', '0039_collectionviewrestriction'),
    ]

    operations = [
        migrations.CreateModel(
            name='DirectoryPage',
            fields=[
                ('page_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='wagtailcore.Page')),
            ],
            options={
                'verbose_name': 'Directory',
            },
            bases=('wagtailcore.page',),
        ),
        migrations.CreateModel(
            name='GroupPage',
            fields=[
                ('page_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='wagtailcore.Page')),
                ('name', models.CharField(max_length=80)),
                ('address1', models.CharField(max_length=80)),
                ('address2', models.CharField(max_length=80)),
                ('district', models.CharField(max_length=80)),
                ('postcode', models.CharField(max_length=6)),
                ('contact', models.CharField(max_length=80)),
                ('phone', models.CharField(blank=True, max_length=20)),
                ('email', models.CharField(blank=True, max_length=60)),
                ('website', models.CharField(blank=True, max_length=60)),
                ('details', wagtail.wagtailcore.fields.RichTextField(blank=True)),
            ],
            options={
                'verbose_name': 'Club/Group',
            },
            bases=('wagtailcore.page',),
        ),
    ]
