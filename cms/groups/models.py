# ------------------------------------------------------------------------------
# Groups models
# ------------------------------------------------------------------------------

from collections import namedtuple
from itertools import groupby
from django.db import models
from wagtail.wagtailcore.models import Page
from wagtail.wagtailcore.fields import RichTextField
from wagtail.wagtailadmin.edit_handlers import FieldPanel, MultiFieldPanel
from wagtail.wagtailsearch import index
from website.models import TopLevelPage

class GroupCategory(models.Model):
    class Meta:
        ordering = ["name"]
        verbose_name = "Group Category"
        verbose_name_plural = "Group Categories"

    code = models.CharField(max_length=4, unique=True)
    name = models.CharField(max_length=80)

    def __str__(self):
        return self.name

    @property
    def item_type(self):
        return "GroupCategory"


class GroupPage(Page):
    class Meta:
        verbose_name = "Club/Group"

    parent_page_types = ["groups.DirectoryPage"]
    subpage_types = ["events.SimpleEventPage", "events.RecurringEventPage"]

    # So that Barney can't add an event for Fred's group
    owner_subpages_only = True

    category = models.ForeignKey(GroupCategory,
                                 related_name="+",
                                 verbose_name="Category",
                                 on_delete=models.SET_NULL,
                                 blank=True, null=True)
    contact = models.CharField(max_length=80, blank=True)
    phone = models.CharField(max_length=30, blank=True)
    email = models.CharField(max_length=60, blank=True)
    alternative_contact = models.CharField(max_length=80, blank=True)
    alternative_phone = models.CharField(max_length=30, blank=True)
    alternative_email = models.CharField(max_length=60, blank=True)
    website = models.URLField(blank=True)
    address1 = models.CharField(max_length=80, blank=True)
    address2 = models.CharField(max_length=80, blank=True)
    town = models.CharField(max_length=80, blank=True)
    postcode = models.CharField(max_length=6, blank=True)
    details  = RichTextField(blank=True)
    last_verified = models.DateField(blank=True, null=True)
    verified_note = models.CharField(max_length=100, blank=True)

    search_fields = Page.search_fields + [
        index.SearchField('town'),
        index.SearchField('contact'),
        index.SearchField('details'),
    ]

    content_panels = Page.content_panels + [
        FieldPanel('category'),
        MultiFieldPanel([
            FieldPanel('contact'),
            FieldPanel('phone'),
            FieldPanel('email')],
            heading="Contact Info", classname="collapsible"),
        MultiFieldPanel([
            FieldPanel('alternative_contact'),
            FieldPanel('alternative_phone'),
            FieldPanel('alternative_email')],
            heading="Alternative Contact Info", classname="collapsible collapsed"),
        FieldPanel('website'),
        MultiFieldPanel([
            FieldPanel('address1'),
            FieldPanel('address2'),
            FieldPanel('town'),
            FieldPanel('postcode')],
            heading="Address", classname="collapsible"),
        FieldPanel('details', classname="full"),
        MultiFieldPanel([
            FieldPanel('last_verified'),
            FieldPanel('verified_note')],
            heading="Verified?", classname=""),
    ]

    @property
    def address(self):
        retval = ", ".join(filter(None, (self.address1,
                                         self.address2,
                                         self.town)))
        if retval and self.postcode:
            retval = "{} {}".format(retval, self.postcode)
        return retval

    @property
    def item_type(self):
        return "GroupPage"


NullGroupCategory = GroupCategory(code="", name="Uncategorised")
DirectorySection = namedtuple("DirectorySection", "category groups")

class DirectoryPage(TopLevelPage):
    class Meta:
        verbose_name = "Directory"

    subpage_types = ['groups.GroupPage']

    content = RichTextField(default='', blank=True)
    content.help_text = "A free-form block of text across the top of the page"

    content_panels = Page.content_panels + [
        FieldPanel('content', classname="full"),
        ]

    @property
    def sections(self):
        groups = GroupPage.objects.live().order_by('category', 'title')
        retval = []
        def getCategory(group):
            if group.category is not None:
                return group.category
            else:
                return NullGroupCategory
        for category, section_groups in groupby(groups, getCategory):
            retval.append(DirectorySection(category, list(section_groups)))
        return retval

    @property
    def columns(self):
        retval = []
        items = []
        groups_count = 0
        categories_count = 0
        groups_per_column = 5
        for section in self.sections:
            items.append(section.category)
            categories_count += 1
            if categories_count >= 2:
                groups_per_column = 4
            for group in section.groups:
                items.append(group)
                groups_count += 1
                if groups_count >= groups_per_column:
                    retval.append(items)
                    items = []
                    groups_count = 0
                    categories_count = 0
                    groups_per_column = 5
        if items:
            retval.append(items)
        return retval

# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------
