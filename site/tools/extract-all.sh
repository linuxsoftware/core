#!/bin/bash
# ===========================================================================
# extract-all.sh
# Copyright 2017, D J Moore (info@linuxsoftware.co.nz)
# ===========================================================================

function extractArchive()
{
    runcmd dar --extract $1 $QDAROPTS $FQDAROPTS
}

HERE=$(realpath $(dirname $0))
source "$HERE/hugs_common.sh"

if [ $# -eq 0 ]; then
    echo "usage: extract-all.sh <archive>" >2
    exit 1
fi
CAP_FOWNER=$(capsh --print|grep "^Current"|grep -o "\<cap_fowner\>")
FQDAROPTS=""
if [ -z $CAP_FOWNER ]; then
    echo "Hint: run as root to restore original file attributes"
    FQDAROPTS="--ignore-owner"
fi

ARCDIR=$(realpath $(dirname $1))
ARCFILE=$(basename ${1%.1.dar})
ARCPATH=$ARCDIR/$ARCFILE
testArchive $ARCPATH
ARCREF=$(dar -q --list $ARCPATH | sed -n 's/User comment   .*"\(-A\|--ref\)" "\([^" ]*\).*/\2/p')
if [ -n $ARCREF ]; then
    ARCFULL=$(basename $ARCREF)
    testArchive $ARCDIR/$ARCFULL
    extractArchive $ARCDIR/$ARCFULL
fi

extractArchive $ARCPATH

