#!/bin/bash
# ===========================================================================
# defrost-inventory.sh
# Copyright 2017, D J Moore (info@linuxsoftware.co.nz)
# ===========================================================================

HERE=$(realpath $(dirname $0))
source "$HERE/hugs_common.sh"

activateVenv

if [ $# -eq 0 ]; then
    echo "Starting inventory"
    aws glacier initiate-job --account-id - --vault-name $VAULT \
        --job-parameters '{"Type": "inventory-retrieval"}'
    echo "You will be emailed when it is ready"
else
    echo "Getting inventory output for job $1"
    aws glacier get-job-output --account-id - --vault-name $VAULT \
        --job-id $1 $VAULT.inventory
fi
