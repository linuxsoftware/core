#!/bin/bash
# ===========================================================================
# CatHugs backup system
# hugs_common.sh
# Copyright 2017, D J Moore (info@linuxsoftware.co.nz)
# ===========================================================================

HERE=$(realpath $(dirname $0))

if [ -f "$HERE/hugs_settings.sh" ]; then
    source "$HERE/hugs_settings.sh"
    : ${VENV:=$PROJ}
    : ${VAULT:=$PROJ}
else
    echo "Cannot find hugs_settings.sh" 1>&2
    exit 1
fi

WEB=$(realpath $HERE/../..)
CMS=$WEB/cms
DATE=$(date -I)
BACKUP=$HOME/backup
QDAROPTS="--quiet -Q --noconf --no-warn"


# Functions

function activateVenv()
{
    if [ -f $HOME/env/$VENV/bin/activate ]; then
        source $HOME/env/$VENV/bin/activate
    fi
}

function runcmd() 
{
    "$@"
    #echo "run: $@"
    local EXIT_CODE=$?

    if [[ $EXIT_CODE -ne 0 ]]; then
        echo "$@ FAILED with exit code $EXIT_CODE"
        exit 1
    fi
}

function saveMachineInfo()
{
    # Generate some info about the state of the machine
    CODE=$CMS/code
    mkdir -p "$CODE"
    runcmd uname -a > "$CODE/$DATE-uname.txt"
    runcmd cp /etc/redhat-release "$CODE/$DATE-osrelease.txt"
    runcmd yum list installed > "$CODE/$DATE-installed.txt" 2>/dev/null
    runcmd pip freeze > "$CODE/$DATE-reqfrozen.txt"
    runcmd git --git-dir=$WEB/.git rev-parse --short HEAD > "$CODE/$DATE-githead.txt"
}

function readDBSettings()
{
    # Read DB settings
    DB=$(sed -n "/DATABASES/,/^\}/{
      /'default'/,/\}/{
      /'default'/b
      /\}/b
      p
      }
    }" < $CMS/cms/settings/local.py)
    DBNAME=$(echo "$DB"|sed -n "s/\ *['\"]NAME['\"]\ *:\ *['\"]\([^'\"]*\)[\"'].*/\\1/p")
    DBUSER=$(echo "$DB"|sed -n "s/\ *['\"]USER['\"]\ *:\ *['\"]\([^'\"]*\)[\"'].*/\\1/p")
    DBPASS=$(echo "$DB"|sed -n "s/\ *['\"]PASSWORD['\"]\ *:\ *['\"]\([^'\"]*\)[\"'].*/\\1/p")
}

function dumpDatabase()
{
    # Dump the database
    readDBSettings
    DATA=$CMS/data
    mkdir -p "$DATA"
    RESTOREUMASK=$(umask -p)
    rm -f "$HOME/.pgpass"
    umask 0077
    echo "localhost:5432:$DBNAME:$DBUSER:$DBPASS" > $HOME/.pgpass
    $($RESTOREUMASK)
    rm -rf "$DATA/$DBNAME.dump"
    runcmd pg_dump --format=directory --username=$DBUSER --create $DBNAME -f "$DATA/$DBNAME.dump"
    rm "$HOME/.pgpass"
}

function restoreDatabase()
{
    # Restore the database
    readDBSettings
    DATA=$CMS/data
    mkdir -p "$DATA"
    RESTOREUMASK=$(umask -p)
    rm -f "$HOME/.pgpass"
    umask 0077
    echo "localhost:5432:$DBNAME:$DBUSER:$DBPASS" > $HOME/.pgpass
    $($RESTOREUMASK)
    runcmd pg_restore --format=directory --username=$DBUSER --create -d $DBNAME  "$DATA/$DBNAME.dump"
    rm "$HOME/.pgpass"
}

function testArchive()
{
    # Test archive
    local ARCDIR=$(dirname $1)
    local ARCFILE=$(basename ${1%.1.dar})
    local ARC1PATH=$ARCDIR/$ARCFILE.1.dar
    if [ ! -f $ARC1PATH ]; then
        echo "Cannot find $ARC1PATH" >2
        exit 1
    fi
    runcmd dar --test $1 $QDAROPTS >/dev/null
    pushd "$ARCDIR" > /dev/null
    for SHA1 in $ARCFILE*.sha1; do
        runcmd sha1sum --status -c $SHA1
    done
    popd > /dev/null
}

function cleanup()
{
    # Cleanup
    rm -f "$CODE/$DATE-uname.txt"
    rm -f "$CODE/$DATE-osrelease.txt"
    rm -f "$CODE/$DATE-installed.txt"
    rm -f "$CODE/$DATE-reqfrozen.txt"
    rm -f "$CODE/$DATE-githead.txt"
    rmdir --ignore-fail-on-non-empty "$CODE"
    rm -rf "$DATA/$DBNAME.dump"
    rmdir --ignore-fail-on-non-empty "$DATA"
}
