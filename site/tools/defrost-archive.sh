#!/bin/bash
# ===========================================================================
# defrost-archive.sh
# Copyright 2017, D J Moore (info@linuxsoftware.co.nz)
# ===========================================================================

HERE=$(realpath $(dirname $0))
source "$HERE/hugs_common.sh"

activateVenv

if [ $# -eq 0 -o $# -gt 2 ]; then
    echo "defrost-archive.sh <archive-id>"
    echo "defrost-archive.sh <job-id> <archive-name>"
elif [ $# -eq 1 ]; then
    echo "Starting defrost of archive $1"
    aws glacier initiate-job --account-id - --vault-name $VAULT \
        --job-parameters "{\"Type\": \"archive-retrieval\",     \
                           \"ArchiveId\": \"$1\"}"
    echo "You will be emailed when it is ready"
elif [ $# -eq 2 ]; then
    echo "Getting archive for job $1"
    aws glacier get-job-output --account-id - --vault-name $VAULT \
        --job-id $1 $2
fi
