#!/bin/bash
# ===========================================================================
# backup-full.sh
# Copyright 2017, D J Moore (info@linuxsoftware.co.nz)
# ===========================================================================

HERE=$(realpath $(dirname $0))
source "$HERE/hugs_common.sh"

activateVenv
dumpDatabase
saveMachineInfo

# Create archive
ARCFULL="$DATE-$PROJ-full"
ARCCAT="$DATE-$PROJ-cat"
mkdir -p "$BACKUP"
runcmd dar --create "$BACKUP/$ARCFULL" -@ "$BACKUP/$ARCCAT" \
    $QDAROPTS --hash=sha1 --alter=atime -s 1G --user-comment "%c" \
    -R "$CMS" -g media -g data -g code

testArchive "$BACKUP/$ARCFULL"
testArchive "$BACKUP/$ARCCAT"
cleanup

# Store in Glacier
for DAR in $BACKUP/$ARCFULL.[0-9]*.dar; do
    TREEHASH=$(treehash $DAR | tee $DAR.treehash | cut -d ' ' -f 2)
    NAME=$(basename $DAR)
    aws glacier upload-archive --account-id - --vault-name $VAULT --body $DAR --archive-description "$NAME" --checksum $TREEHASH > $DAR.glacier 2>&1
done
