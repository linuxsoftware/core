#!/bin/bash
# ===========================================================================
# backup-diff.sh
# Copyright 2017, D J Moore (info@linuxsoftware.co.nz)
# ===========================================================================

HERE=$(realpath $(dirname $0))
source "$HERE/hugs_common.sh"

activateVenv
dumpDatabase
saveMachineInfo

# Find most recent full backup
DATEMASK="20[1-9][0-9]-[01][0-9]-[0-3][0-9]"
LASTFILE=$(ls -1tr $BACKUP/$DATEMASK-$PROJ-full.1.dar | tail -n 1)
ARCLASTFULL=$(basename $LASTFILE | sed 's/\.1\.dar//')

# Create archive
ARCDIFF="$DATE-$PROJ-diff"
ARCCAT="$DATE-$PROJ-cat"
mkdir -p "$BACKUP"
runcmd dar --create "$BACKUP/$ARCDIFF" -A "$BACKUP/$ARCLASTFULL" -@ "$BACKUP/$ARCCAT" \
    $QDAROPTS --hash=sha1 --alter=atime -s 1G --user-comment "%c" \
    -R "$CMS" -g media -g data -g code

testArchive "$BACKUP/$ARCDIFF"
testArchive "$BACKUP/$ARCCAT"
cleanup
