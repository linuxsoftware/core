#!/bin/bash
# ===========================================================================
# listbackups.sh
# Copyright 2017, D J Moore (info@linuxsoftware.co.nz)
# ===========================================================================

HERE=$(realpath $(dirname $0))
source "$HERE/hugs_common.sh"

activateVenv

aws glacier list-vaults --account-id - --no-paginate --output text --query 'VaultList[*].[VaultName]'

